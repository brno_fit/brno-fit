from __future__ import unicode_literals
from django.contrib.sessions.models import Session
from django.db import models

from trainer.models import Trainer
from django.utils.translation import ugettext as _

RATING = (
    (1, _('Bad')),
    (2, _('Normal')),
    (3, _('Good')),
    (4, _('Very good')),
    (5, _('Best'))
)


class Review(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    description = models.TextField()
    rate = models.IntegerField(choices=RATING, default=3)
    trainer = models.ForeignKey(
        Trainer, on_delete=models.CASCADE, related_name='reviews')
    created = models.DateField(auto_now_add=True)
    session = models.ForeignKey(Session)

    def __unicode__(self):
        return self.trainer.user.username + ' ' + str(self.rate)

    class Meta:
        ordering = ['-created']