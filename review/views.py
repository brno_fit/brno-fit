from django.shortcuts import get_object_or_404, render_to_response
from django.views.generic.edit import CreateView
from django.http import Http404, JsonResponse
from django.template.loader import get_template
from django.template import Context
from django.contrib.sessions.models import Session
from django.utils.translation import ugettext as _

from models import Review
from forms import ReviewForm

from trainer.models import Trainer
from support.functions import get_or_none


class CreateReviewView(CreateView):
    model = Review
    form_class = ReviewForm
    template_name = 'review/create.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CreateReviewView, self).get_context_data(*args, **kwargs)
        context['pk'] = self.kwargs.get('pk', None)
        return context

    def form_valid(self, form):
        trainer_id = self.kwargs.get('pk', None)
        if not trainer_id:
            raise Http404('Can\'t find trainer with this id')
        trainer = get_object_or_404(Trainer, pk=trainer_id)
        session = get_or_none(Session, session_key=self.request.session.session_key)
        if not session:
            return JsonResponse({
                    'saved' : False,
                    'message' : _('Can\'t save review')
                })
        
        if Review.objects.filter(session=session, trainer=trainer).exists():
            return  JsonResponse({
                        'saved' : False, 
                        'message' : _('You can\'t add review on same trainer twice.') 
                    })
        form.instance.trainer = trainer
        form.instance.session = session
        review = form.save()
        
        review_template = get_template('review/review.html')

        return JsonResponse({
            'saved' : True, 
            'review' : review_template.render(Context({'review' : review})),
            'message' : _('Review saved')
        })

    def form_invalid(self, form):
        return JsonResponse({
            'saved' : False, 
            'message' : _('Please, check all fields.'),
            'errors' : str(form.errors)
        })

    def get_success_url(self, *args, **kwargs):
        return self.redirect(request.META['HTTP_REFERER'])