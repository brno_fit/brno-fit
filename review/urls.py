from django.conf.urls import url
from views import CreateReviewView 

urlpatterns = [
    url(r'create/(?P<pk>[\d]+)/$', CreateReviewView.as_view(), name='create'),
]
