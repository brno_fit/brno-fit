from __future__ import unicode_literals

from django.db import models
from gym.models import Gym
from trainer.models import Trainer

NOTIFICATION_TYPES = (
    (0, 'trainer to gym'),
    (1, 'gym to trainer')
)


class Notification(models.Model):
    gym = models.ForeignKey(Gym)
    trainer = models.ForeignKey(Trainer)
    ntype = models.IntegerField(choices=NOTIFICATION_TYPES)

    def __unicode__(self):
        if self.ntype:
            return '%s to %s' % (self.gym.name, self.trainer.user.username)
        else:
            return '%s to %s' % (self.trainer.user.username, self.gym.name)
