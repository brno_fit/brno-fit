from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from models import Notification
from support.functions import get_or_none
from gym.models import Gym
from trainer.models import Trainer

# ajax


@login_required
def invite(request, gym_pk, trainer_pk, ntype):
    gym = get_or_none(Gym, pk=gym_pk)
    trainer = get_or_none(Trainer, pk=trainer_pk)
    if gym and trainer and not get_or_none(Notification, gym=gym, trainer=trainer, ntype=ntype):
        Notification.objects.create(
            gym=gym, trainer=trainer, ntype=ntype).save()
    else:
        return HttpResponse('error')
    return HttpResponse('invited')


@login_required
def submit(request, pk):
    n = get_or_none(Notification, pk=pk)
    if n:
        n.gym.trainers.add(n.trainer)
        n.delete()
        return HttpResponse('submited')
    return HttpResponse('error')


@login_required
def cancel(request, pk):
    n = get_or_none(Notification, pk=pk)
    if n:
        n.gym.trainers.remove(n.trainer)
        n.delete()
        return HttpResponse('canceled')
    return HttpResponse('error')
