from django.conf.urls import url
from views import invite, submit, cancel

urlpatterns = [
    url(r'(?P<gym_pk>[\d]+)/(?P<trainer_pk>[\d]+)/(?P<ntype>[\d]+)$',
        invite, name='invite'),
    url(r'submit/(?P<pk>[\d]+)$', submit, name='submit'),
    url(r'cancel/(?P<pk>[\d]+)$', cancel, name='cancel'),
]
