$( document ).ready(function () {
    $(".change_lang").on('click', function(event){
        if (window.location.hash) {
            url = $(this).attr('href');
            $(this).attr('href', url + window.location.hash);
        }
    });

    $("select").addClass("ui fluid search dropdown");
    $(".dropdown").dropdown();

    $(".set-avatar").on("click", function (event) {
        event.preventDefault();
        var avatar = $(this);  
        $.get({
            url: $(this).attr("href")
        }).done(function(data) {
            $('a.label.green').removeClass("green");
            avatar.addClass("green");
        });
    });

    $( ".delete-link" ).on("click", function(event) {
        event.preventDefault();
        $.get({
            url: $(this).attr("href")
        });
        var id = $(this).attr("delete-id");
        $( "#" + id ).transition('fade');
    });

    $('.message .close').on('click', function() {
        $(this).closest('.message').transition('fade');
    });

    $('.menu .item').tab({
        history : true
    });
    
    $(':input').focus(function(){
        $(this).parents('.field').removeClass("error");
    });

    $('.ui.rating').rating('disable');

    $('a').popup();

    $('.modal-gym').on("click", function (event){
        event.preventDefault();
        var modal = $('#' + $(this).attr('modal-id'));
        var id = $(this).attr('gym-id');
        $.get({
            url : $(this).attr('href')
        }).done(function (data){
            modal.find('.grid').html(data);
            modal.modal('show');
            initWithPoint(id);
            modal.find('.fotorama').fotorama();
        });
    });

    $('.fotorama').fotorama();

    $('#invite').on("click", function(event){
        event.preventDefault();
        var t = $(this);
        $.get({
            url : $(this).attr('href'),
            data : { 'csrfmiddlewaretoken' : $('input[name="csrfmiddlewaretoken"]').val() }
        }).done(function(data){
            t.parents('.row').hide("slow");
        });
    });

    $('.refresh').on("click", function (event) {
        event.preventDefault();
        $.get({
            url : $(this).attr('href')
        }).done(function(data){
            location.reload();
        });
    });
    
    $('.ui.accordion').accordion({
        'exclusive' : false
    });
});