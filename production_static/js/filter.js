var map, marker;

var gmarkers1 = [];

function fadeAlert() {
    $("#filter_alert").transition('fade');
}

$(document).ready(function () {

    initMap();

    var gym_item = null;

    $.get('/static/templates/gym/gym_item.html', function (data) {
        gym_item = $(data);
        $.ajax({
            type: 'GET',
            url: '/gym/json'
        }).done(function (data) {
            for (var i in data.data) {
                if (i !== undefined) {
                    var temp_item = gym_item.clone();
                    $(temp_item).find("a.item").attr("href", "/gym/detail/"+data.data[i].pk);
                    $(temp_item).find("img").attr("src", data.data[i].avatar);
                    $(temp_item).find(".header").text(data.data[i].name);                            
                    $(temp_item).find(".meta span").text(data.data[i].address.country+", "+data.data[i].address.city+", "+data.data[i].address.street);                            
                    // $("#gym_list").append("<a class=\"item inline-item\" href=\"/gym/detail/"+data.data[i].pk+"\">"+
                    //         "<div class=\"ui small fill-image\"><img src=\""+data.data[i].avatar+"\"></div>"+
                    //         "<div class=\"middle aligned content\">"+
                    //           "<div class=\"header\">"+data.data[i].name+"</div>"+
                    //           "<div class=\"meta\">"+
                    //           " <span>"+data.data[i].address.country+", "+data.data[i].address.city+", "+data.data[i].address.street+"</span>"+
                    //           "</div>"+
                    //           "<div class=\"description\">"+
                    //           "  <p></p>"+
                    //           "</div>"+
                    //         "</div>"+
                    //     "</a>");
                    $("#gym_list").append(temp_item);
                }
            }
            if ($("#gym_list").text().length == 0) {
                $("#gym_list").append("No results found");
            }
        });
    });
    // $("#filter_alert").css('visibility', 'hidden');
    $('#filter_alert .close')
        .on('click', function() {
            $('#filter_alert').removeClass("my-visible");
            $('#filter_alert').addClass("my-hidden");
            setTimeout(function (obj) {
                // document.getElementById("filter_alert").style.display = "none";
                $(obj).removeClass("hidden");
                $(obj).removeClass("transition");}, 1000, $('#filter_alert'));
        });
});

function initMap() {
    var point = {lat: 0, lng: 0};
    map = new google.maps.Map(document.getElementById('map_filter'), {
        center: point,
        zoom: 2,
        disableDefaultUI: true
    });
    google.maps.event.addListener(map, 'dragend', function() {
        filter();
    });
    google.maps.event.addListener(map, 'zoom_changed', function() {
        filter();
    });
    $("#map_filter").css('visibility', 'hidden');
}

function sendFilterRequest() {
    var $inputs = $('#gym_filter_form :input');

    var parameters = {};
    $inputs.each(function() {
        if ($(this).val()) {
            parameters[this.name] = $(this).val();
        }
    });

    if (Object.getOwnPropertyNames(parameters).length <= 1) {

        $('#filter_alert').removeClass("my-hidden");

        return;
    }

    $("#map_filter").css('visibility', 'visible');
    
    $.ajax({
        type: 'POST',
        url: '/gym/json',
        data: parameters
    }).done(function (data) {
        for(var i in gmarkers1) { // looping through my Markers Collection
            if (i !== undefined) {
                gmarkers1[i].setMap(null);
            }
        }
        $.each(data.data, function (index, gym) {
            console.log(gym.pk);
            if (gmarkers1[gym.pk] === undefined) {
                setMarker(gym);
            }
            gmarkers1[gym.pk].data = gym;
            gmarkers1[gym.pk].title = gym.name;
            gmarkers1[gym.pk].setMap(map);        
        });
        if (data.data[0] !== undefined) {
            map.setCenter({lat: data.data[0].address.coords[0], lng: data.data[0].address.coords[1]});
            map.setZoom(5);
        }
        filter();
        if ($("#gym_list").text().length == 0) {
            $("#gym_list").append("No results found");
        }
    });
}

function filter() {
    $("#gym_list").empty();
    var bounds = map.getBounds();
    for (var i in gmarkers1) {
        if (i !== undefined && bounds.contains(gmarkers1[i].position)) {
            $("#gym_list").append("<a class=\"item inline-item\" href=\"/gym/detail/"+gmarkers1[i].data.pk+"\">"+
                    "<div class=\"ui small fill-image\"><img src=\""+gmarkers1[i].data.avatar+"\"></div>"+
                    "<div class=\"middle aligned content\">"+
                      "<div class=\"header\">"+gmarkers1[i].data.name+"</div>"+
                      "<div class=\"meta\">"+
                      " <span>"+gmarkers1[i].data.address.country+", "+gmarkers1[i].data.address.city+", "+gmarkers1[i].data.address.street+"</span>"+
                      "</div>"+
                      "<div class=\"description\">"+
                      "  <p></p>"+
                      "</div>"+
                    "</div>"+
                "</a>");
        }
    }
}

function setMarker(marker) {
    var id = marker.pk;
    var title = marker.name;
    var pos = new google.maps.LatLng(marker.address.coords[0], marker.address.coords[1]);

    marker1 = new google.maps.Marker({
        id: id,
        title: title,
        position: pos,
        data: marker,
        map: map
    });

    marker1.addListener('click', function() {
        var contentString = this.title;

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        infowindow.open(map, this);
    });


    gmarkers1[marker1.id] = marker1;
}