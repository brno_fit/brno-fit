$( document ).ready(function () {
    var map, geocoder, marker;
    var initialized = false;

    $('.preregister').submit(function(event){
        preRegister();
        return true;
    });
    
    document.getElementById('control_address').addEventListener('click', function(event) {
     	event.preventDefault();
     	initMap();
        geocodeAddress(geocoder, map);
    });

    if ($('.map.init').length) {
        initMap();
        geocodeAddress(geocoder, map);
    }

    $('#control_address').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        if (!initialized){
            initMap();
        } 
        geocodeAddress(geocoder, map);
    });

    function initMap() {
        initialized = true;
        var point = {lat: 0, lng: 0};
        map = new google.maps.Map(document.getElementsByClassName('map')[0], {
            center: point,
            zoom: 15,
            disableDefaultUI: true
        });
        geocoder = new google.maps.Geocoder();
    }

    function geocodeAddress(geocoder, resultsMap) {
        var country, city, street, postcode;
        country = $("#id_country option:selected").text();
        city = $("#id_city option:selected").text();
        street = $('#id_street');
        var address = country + ', ' + city + ', ' + street.val();
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable:true
                });
                map.setCenter(results[0].geometry.location);
                map.setZoom(15);
                marker.addListener('dragend', function(event) {
                    $('#id_point').value = marker.getPosition().lat() + ' ' + marker.getPosition().lng();
                    geocoder.geocode({'location': {lat: marker.getPosition().lat(), lng: marker.getPosition().lng()}}, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var street_found = false;
                            var number_found = false;
                            if (results.length) {
                                street.val('');
                                var str_num = "";
                                var str_name = "";
                                for (var i in results[0].address_components) {
                                    if (results[0].address_components[i].types.indexOf("route") > -1) {
                                        str_name = results[0].address_components[i].long_name;
                                        street_found = true;
                                    }

                                    if (results[0].address_components[i].types.indexOf("street_number") > -1) {
                                        str_num = results[0].address_components[i].long_name;
                                    }
                                }
                                street.val(str_name + " " + str_num);
                            }
                        } else {
                            console.log('Geocoder failed due to: ' + status);
                        }
                    });
                });
                $('#id_point').val(marker.getPosition().lat() + ' ' + marker.getPosition().lng());
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function preRegister() {
        var country, city, street;
        country = $("#id_country option:selected").text();
        city = $("#id_city option:selected").text();
        street = $('#id_street').val();
        var address = country + ', ' + city + ', ' + street;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                $('#id_point').val(results[0].geometry.location.lat() + ' ' + results[0].geometry.location.lng());
                console.log($('#id_point').val());
            }
        });
    }
});