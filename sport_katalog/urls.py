"""sport_katalog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.conf.urls import url, include
	2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from trainer.views import TrainerRegisterView
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

from django.conf.urls.i18n import i18n_patterns

urlpatterns = i18n_patterns(
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', admin.site.urls),
    url(r'^allauth/', include('allauth.urls')),
    url(r'^', include('support.urls', namespace='support')),
    url(r'^trainer/', include('trainer.urls', namespace='trainer')),
    url(r'^gym/', include('gym.urls', namespace='gym')),
    url(r'^training_partner/', include('training_partner.urls', namespace='training_partner')),
    url(r'^invite/', include('invite.urls', namespace='invite')),
    url(r'^payment/', include('payment.urls', namespace='payment')),
    url(r'^message/', include('message.urls', namespace='message')),
    url(r'^review/', include('review.urls', namespace='review')),
    url(r'^openhours/', include('openhours.urls', namespace='openhours')),
    url(r'^google64a2aa86f84ef7bf.html', TemplateView.as_view(template_name='google64a2aa86f84ef7bf.html')),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
