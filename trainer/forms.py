from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from phonenumber_field.formfields import PhoneNumberField
from cities.models import Country, City

from support.forms import CityField
from models import Trainer

class TrainerRegisterForm(forms.ModelForm):
    city = CityField(queryset=City.objects.none())

    class Meta:
        model = Trainer
        fields = ['first_name', 'last_name',
                  'country', 'certificate', 'phone', 'city']

class TrainerAdminForm(forms.ModelForm):
    city = CityField(queryset=City.objects.none())

    class Meta:
        model = Trainer
        fields = '__all__'

class TrainerFilterForm(forms.Form):
    name = forms.CharField(
        max_length=30, 
        widget=forms.TextInput(attrs={'placeholder': _('Gym name')}),
        required=False
    )
    country = forms.ModelChoiceField(
        queryset=Country.objects, 
        to_field_name="pk", empty_label=_("Country"),
        required=False
    )
    city = forms.ModelChoiceField(
        queryset=City.objects.none(), 
        empty_label=_("Please, pick a country"),
        required=False
    )