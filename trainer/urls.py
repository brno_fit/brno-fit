from django.conf.urls import url
from views import (
    TrainerProfile, TrainerProfileUpdate,
    photo_save, photo_delete, TrainerRegisterView
)

urlpatterns = [
    url(r'detail/(?P<pk>[\d]+)/$', TrainerProfile.as_view(), name='detail'),
    url(r'settings/$', TrainerProfileUpdate.as_view(), name='settings'),
    url(r'register/$', TrainerRegisterView.as_view(), name='register'),

    # ajax
    url(r'photo/save/$', photo_save, name='photo_save'),
    url(r'photo/delete/(?P<pk>[\d]+)/$', photo_delete, name='photo_delete'),
]
