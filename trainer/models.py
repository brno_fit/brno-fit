from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from cities.models import Country, City
from phonenumber_field.modelfields import PhoneNumberField

from imagekit.models import ProcessedImageField


class Trainer(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='trainer')
    first_name = models.CharField(max_length=50, blank=False, null=False)
    last_name = models.CharField(max_length=50)
    country = models.ForeignKey(Country)
    city = models.ForeignKey(City)
    phone = PhoneNumberField()
    certificate = models.CharField(max_length=50, null=True, blank=True)
    can_show = models.BooleanField(default=True)
    
    description = models.TextField(null=True, blank=True, verbose_name='About me')
    rate = models.FloatField(default=0.0)

    def __unicode__(self):
        return self.user.username


class TrainerPhoto(models.Model):
    trainer = models.ForeignKey(Trainer, related_name='photos')
    photo = ProcessedImageField(
        upload_to='client/', format='JPEG', options={'quality': 60})
    avatar = models.BooleanField(default=False)

    def __unicode__(self):
        return self.trainer.user.username
