from django.contrib import admin
from models import Trainer, TrainerPhoto
from forms import TrainerAdminForm


class TrainerAdmin(admin.ModelAdmin):
	form = TrainerAdminForm

	class Media:
		js = ('js/cities.js',)

class TrainerPhotoAdmin(admin.ModelAdmin):
    pass

admin.site.register(Trainer, TrainerAdmin)
admin.site.register(TrainerPhoto, TrainerPhotoAdmin)
