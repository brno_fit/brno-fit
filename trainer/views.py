from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic.detail import DetailView
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.utils.translation import ugettext as _

from support.forms import UserProfileForm
from forms import TrainerRegisterForm
from cities.models import City, Country
from models import Trainer, TrainerPhoto
from support.functions import get_or_none
from review.forms import ReviewForm
from invite.models import Notification
from gym.models import Gym
from message.forms import MessageCreateForm


class TrainerRegisterView(LoginRequiredMixin, CreateView):
    template_name = 'trainer/register.html'
    model = Trainer
    form_class = TrainerRegisterForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(TrainerRegisterView, self).form_valid(form)

    def get_success_url(self):
        return reverse('trainer:settings')


class TrainerProfile(DetailView):
    model = Trainer
    template_name = 'trainer/detail.html'
    review_form = ReviewForm
    message_form = MessageCreateForm

    def get_context_data(self, **kwargs):
        context = super(TrainerProfile, self).get_context_data(**kwargs)
        context['review_form'] = self.review_form()
        context['message_form'] = self.message_form()
        try:
            context['rating'] = sum(self.object.reviews.values_list(
                'rate', flat=True)) / self.object.reviews.count()
        except ZeroDivisionError:
            context['rating'] = 0
        user = self.request.user
        if user.is_authenticated():
            if (getattr(user, 'gym', None) 
                and not get_or_none(Notification, gym=user.gym, trainer=self.object, ntype=1) 
                and not user.gym.trainers.filter(pk=self.object.pk).exists()):
                context['invite'] = True
        return context

    def post(self, request, pk):
        self.object = self.get_object()
        context = self.get_context_data(pk=pk)
        review_form = self.review_form(request.POST)
        context['review_form'] = review_form
        if review_form.is_valid():
            rate_sum = float(sum(self.object.reviews.values_list(
                'rate', flat=True)) + review_form.cleaned_data['rate'])

            print rate_sum
            self.object.rate = (((self.object.reviews.count(
            ) + 1) / (6 - rate_sum / (self.object.reviews.count() + 1))))
            print self.object.rate
            self.object.save()
            review = review_form.save(commit=False)
            review.trainer = self.object
            review.save()
            return redirect('trainer:detail', pk=pk)
        return render(request, self.template_name, context)


class TrainerProfileUpdate(LoginRequiredMixin, UpdateView):
    login_url = 'account_login'
    model = Trainer
    template_name = 'trainer/settings.html'
    form_class = TrainerRegisterForm
    success_url = 'trainer:settings'
    user_form = UserProfileForm

    def get_context_data(self, **kwargs):
        context = super(TrainerProfileUpdate, self).get_context_data(**kwargs)
        context['my_city'] = self.object.city
        context['user_form'] = self.user_form(instance=self.object.user)
        # ntype 1 is gym to trainer
        context['invites'] = Notification.objects.filter(
            trainer=self.object, ntype=1)
        return context

    def get_object(self):
        return Trainer.objects.get(user=self.request.user)

    def form_valid(self, form):
        if 'cancel' in self.request.POST:
            return redirect('trainer:detail', pk=pk)
        if self.request.POST.get('city', None):
            form.instance.city = get_or_none(City, pk=self.request.POST.get('city'))
        form.save()
        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse(self.success_url)


@login_required
def photo_save(request):
    if request.user.trainer.photos.count() >= 5:
        return HttpResponse(_('Max is 5'))
    avatar = get_or_none(
        TrainerPhoto, trainer=request.user.trainer, avatar=True)
    photo = TrainerPhoto(
        trainer=request.user.trainer, photo=request.FILES['file'])
    if not avatar:
        photo.avatar = True
    photo.save()
    return HttpResponse('Uploaded')


@login_required
def photo_delete(request, pk):
    photo = get_or_none(Photo, trainer=request.user.trainer, pk=pk)
    if photo:
        photo.delete()
        return HttpResponse('Deleted')
    return HttpResponse('Photo does not exist')
