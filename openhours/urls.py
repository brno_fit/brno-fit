from django.conf.urls import url
from views import OpenHoursView

urlpatterns = [
    url(r'save$', OpenHoursView.as_view(), name='save'),
]
