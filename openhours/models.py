from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.db import models
from gym.models import Gym

WEEKDAYS = [
    (1, _("Monday")),
    (2, _("Tuesday")),
    (3, _("Wednesday")),
    (4, _("Thursday")),
    (5, _("Friday")),
    (6, _("Saturday")),
    (7, _("Sunday")),
]

class OpenHours(models.Model):
    gym = models.ForeignKey(Gym)
    weekday = models.IntegerField(choices=WEEKDAYS, default=_("Monday"))
    from_hour = models.TimeField()
    to_hour = models.TimeField()

    class Meta:
        unique_together = ['gym', 'weekday']
        ordering = ['weekday']

    def __unicode__(self):
        return '%s %s %s %s' % (self.gym, self.weekday, self.from_hour, self.to_hour)