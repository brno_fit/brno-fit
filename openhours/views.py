from django.shortcuts import render, redirect
from django.views.generic.base import TemplateView
from django.forms import modelformset_factory
from django.http import HttpResponse

from models import OpenHours
from forms import OpenHoursForm


class OpenHoursView(TemplateView):
    template_name = 'gym/settings/openhours.html'

    def post(self, request):
        HoursFormset = modelformset_factory(OpenHours, exclude=['gym',])
        formset = HoursFormset(request.POST)
        if formset.is_valid():
            instances = formset.save()
            return HttpResponse('Saved')
        return render(request, self.template_name, {'hours_form' : formset})