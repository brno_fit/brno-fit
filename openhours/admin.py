from django.contrib import admin

from models import OpenHours


class OpenHoursAdmin(admin.ModelAdmin):
	pass

admin.site.register(OpenHours, OpenHoursAdmin)