from django import forms

from models import OpenHours


class OpenHoursForm(forms.ModelForm):

    class Meta:
        model = OpenHours
        fields = '__all__'
        exclude = ['gym']