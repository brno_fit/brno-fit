if (!$) {
    $ = django.jQuery;
}
$( document ).ready(function () {
	if ($('#id_country').length && $( "#id_country" ).val().length) {
		load_cities();
	}

	$( "#id_country" ).change(function (event) {
		load_cities();
	});

	function load_cities(){
		var country = $('#id_country');
		$( "#id_city" ).empty();
		$.ajax({
			type : 'get',
			url : '/get_cities/',
			data : {'pk' : country.val()}
		}).done(function (data) {
			$.each(data.data, function (index, city) {
				$( "#id_city" ).append('<option value="' + city[0] + '">' + city[1] + '</option>');
			});
			$("#id_city").val("");
		});
	}
});