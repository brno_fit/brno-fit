from django.shortcuts import render, redirect
from django.views.generic.detail import DetailView
from training_partner.forms import (TrainingPartnerForm,
                                    TrainingPartnerFilterForm)
from models import TrainingPartner
from django.db.models import Q
from support.views import ListView
from django.template.loader import render_to_string
from django.template import RequestContext
from django.http import HttpResponse
from support.functions import get_or_none


def trainer_partner_filter(get):
    default_queryset = TrainingPartner.objects.all()
    fquery = Q()
    if get.get('name', None):
        fquery = Q(first_name__icontains=get['name'])
        fquery = fquery | Q(last_name__icontains=get['name'])
    if get.get('country', None):
        fquery = fquery & Q(country=get['country'])
    if get.get('city', None):
        fquery = fquery & Q(city=get['city'])
    return default_queryset.filter(fquery)


class TrainingPartnerView(DetailView):
    """docstring for TrainingPartnerView"""
    training_partner_form = TrainingPartnerForm
    filter_form = TrainingPartnerFilterForm
    template_name = 'training_partner/main.html'

    def get(self, request):
        context = self.get_context_data(request=request)
        return render(request, self.template_name, context)

    def get_context_data(self, request):
        context = dict()
        context['register_form'] = self.training_partner_form()
        context['trainer_partner_list'] = trainer_partner_filter(request.GET)
        context['trainer_partner_list'] = ListView(TrainingPartner).get_list(
            request.GET, context['trainer_partner_list'])
        context['filter_form'] = self.filter_form()

        return context

    def post(self, request):
        training_form = self.training_partner_form(request.POST)
        if training_form.is_valid():
            training_form.save()
            return redirect('training_partner:list')
        context = self.get_context_data(request)
        return render(request, self.template_name, context)


class TrainingPartnerProfile(DetailView):
    template_name = 'training_partner/detail.html'
    model = TrainingPartner
    fields = '__all__'

    def get(self, request, pk):
        if request.is_ajax():
            self.object = self.get_object()
            context = self.get_context_data(pk=pk)
            html = render_to_string('training_partner/content_detail.html', context)
            return HttpResponse(html)

class TrainingPartnerRegisterView(DetailView):
    register_form = TrainingPartnerForm
    template_name = 'training_partner/register_content.html'
    fields = '__all__'
    model = TrainingPartner

    def get_context_data(self):
        context = dict()
        context['register_form'] = self.register_form()
        return context

    def get(self, request):
        context = self.get_context_data()
        # html = render_to_string('training_partner/register_content.html', context, context_instance=RequestContext(request))
        return render(request, 'training_partner/register_content.html', context)


# class TrainingPartnerRegisterView(TemplateView):
#     forms = {
#         'form': TrainingPartnerForm
#     }
#     template_name = 'training_partner/register_content.html'

#     def get_context_data(self, **kwargs):
#         context = super(TrainingPartnerRegisterView, self).get_context_data(**kwargs)
#         for name, form in self.forms.iteritems():
#             if self.request.POST:
#                 context[name] = form(self.request.POST)
#             else:
#                 context[name] = form()
#         return context

#     def post(self, request):
#         context = self.get_context_data()
#         errors = False
#         for name, form in self.forms.iteritems():
#             context[name] = form(request.POST)
#             if not context[name].is_valid():
#                 errors = True
#         if errors:
#             return render(request, self.template_name, context)
#         else:
#             training_partner_form = context['form']
#             training = TrainingPartner(
#                 email=gym_form.cleaned_data['email'],
#                 description=gym_form.cleaned_data['description'],
#                 first_name=gym_form.cleaned_data['first_name'],
#                 last_name=gym_form.cleaned_data['last_name'],
#                 last_name=gym_form.cleaned_data['last_name'],
#             )
#             user.save()
#             point = request.POST.get('point').split()
#             address = Address(
#                 country=address_form.cleaned_data['country'],
#                 city=get_or_none(City, pk=address_form.cleaned_data['city']),
#                 street=address_form.cleaned_data['street'],
#                 number=address_form.cleaned_data['number'],
#                 postcode=address_form.cleaned_data['postcode'],
#                 point=Point(float(point[0]), float(point[1]))
#             )
#             address.save()
#             gym = Gym(
#                 name=gym_form.cleaned_data['name'],
#                 user=user,
#                 address=address
#             ).save()
#         return redirect('accounts:auth_login')
