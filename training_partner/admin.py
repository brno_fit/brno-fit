from django.contrib import admin
from models import TrainingPartner


class TrainingPartnerAdmin(admin.ModelAdmin):
	pass


admin.site.register(TrainingPartner, TrainingPartnerAdmin)
