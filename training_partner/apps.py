from __future__ import unicode_literals

from django.apps import AppConfig


class TrainingPartnerConfig(AppConfig):
    name = 'training_partner'
