from django import forms
from models import TrainingPartner
from cities.models import Country, City
from support.functions import get_or_none


class CityField(forms.ChoiceField):
    def validate(self, value):
        if get_or_none(City, pk=value):
            return
        raise forms.ValidationError(_('City is required'))


class TrainingPartnerForm(forms.ModelForm):
    city = CityField(choices=())

    class Meta:
        model = TrainingPartner
        # fields = ['first_name', 'last_name', 'description', 'email', 'phone', 'country', 'city']
        fields = '__all__'
        exclude = ['city']


class TrainingPartnerFilterForm(forms.Form):
    name = forms.CharField(
        max_length=30,
        widget=forms.TextInput(attrs={'placeholder': 'Partner name'}
                               )
    )
    country = forms.ModelChoiceField(
        queryset=Country.objects, to_field_name="pk", empty_label="Country")
    city = forms.ModelChoiceField(
        queryset=City.objects.none(), empty_label="Please, pick a country")
