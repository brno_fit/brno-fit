from __future__ import unicode_literals

from django.db import models
from cities.models import Country, City
from phonenumber_field.modelfields import PhoneNumberField


class TrainingPartner(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    city = models.ForeignKey(City, null=True, blank=True)
    phone = PhoneNumberField(null=True, blank=True)
    description = models.TextField()
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    email = models.CharField(max_length=50)

    def __unicode__(self):
        return self.last_name + " " + self.first_name

    class Meta:
        ordering = ['created']
