from django.conf.urls import url
from views import (
    TrainingPartnerProfile, TrainingPartnerView, TrainingPartnerRegisterView
)

urlpatterns = [
    url(r'list$', TrainingPartnerView.as_view(), name='list'),
    url(r'register$', TrainingPartnerRegisterView.as_view(), name='register'),
    url(r'detail/(?P<pk>[\d]+)/$', TrainingPartnerProfile.as_view(), name='detail'),
]
