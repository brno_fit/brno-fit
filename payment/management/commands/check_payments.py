from django.core.management.base import BaseCommand, CommandError
from payment.models import Payment, PaidTariff
import braintree
from datetime import datetime, timedelta
from django.conf import settings


def get_tariff_length(ttype):
    return settings.TARIFF.get(ttype)['days']

class Command(BaseCommand):
    help = 'Check payment and create tariff if payment was settled'

    def handle(self, *args, **options):
        payments = Payment.objects.filter(status='submitted_for_settlement')
        print payments

        for payment in payments:
            try:
                transaction = braintree.Transaction.find(payment.transaction_id)
            except braintree.exceptions.not_found_error.NotFoundError as e:
                raise e

            if transaction.status == 'settled':
                payment.status = 'settled'
                payment.save()
<<<<<<< HEAD
                # take user tariffs which are later than now
                # if exist, connect it with latest
                utariffs = PaidTariff.objects.filter(user=payment.user, valid_to__gte=datetime.now())
                if utariffs.exists():
                    valid_from = utariffs.latest('valid_to').valid_to
                    actual = False
                else:
                    valid_from = datetime.now()
                    actual = True
                valid_to = valid_from + timedelta(get_tariff_length(payment.tariff_type))
                tariff = PaidTariff.objects.create(
                    payment=payment,
                    tariff_type=payment.tariff_type,
                    valid_from=valid_from, 
                    valid_to=valid_to,
                    user=payment.user,
                    actual=actual
=======
                utariffs = PaidTariff.objects.filter(user=payment.user)
                valid_from = datetime.now()
                valid_to = valid_from + timedelta(get_tariff_length(payment.tariff_type))
                if utariffs.exists():
                    latest = utariffs.latest('valid_to')
                valid_from = latest.valid_to
                valid_to = valid_from + timedelta(get_tariff_length(payment.tariff_type))
                tariff = PaidTariff.objects.create(payment=payment, 
                    valid_from=valid_from, 
                    valid_to=valid_to,
                    user=payment.user
>>>>>>> f05861d027f63da359789129484f87ba03d5c6a6
                )