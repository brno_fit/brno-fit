from django.core.management.base import BaseCommand, CommandError
from payment.models import PaidTariff
from datetime import datetime, timedelta
from django.conf import settings


class Command(BaseCommand):
    help = 'Check tariffs and set can_show attribute for trainer/gym'

    def handle(self, *args, **options):
        old_actual = PaidTariff.objects.filter(actual=True)

        now = datetime.now().date()

        for tariff in old_actual:
            if tariff.valid_to < now:
                tariff.actual = False
                tariff.save()
                try:
                    tariff.user.trainer.can_show = False
                    tariff.user.trainer.save()
                except Exception, e:
                    # if gym does exist, we have a problem
                    # user paid for tariff but don't have trainer/gym
                    try:
                        tariff.user.gym.can_show = False
                        tariff.user.gym.save()
                    except Exception, e:
                        pass

        real_actual = PaidTariff.objects.filter(valid_from__lte=now, valid_to__gte=now)

        for tariff in real_actual:
            if not tariff.actual:
                tariff.actual = True
                tariff.save()
            try:
                tariff.user.trainer.can_show = True
                tariff.user.trainer.save()
            except Exception, e:
                try:
                    tariff.user.gym.can_show = True
                    tariff.user.gym.save()
                except Exception, e:
                    pass
