from django.contrib import admin
from models import ClientToken, Payment, PaidTariff
from django.conf import settings

class PaymentAdmin(admin.ModelAdmin):
    fields = [
        'transaction_braintree_url',
        'tariff_type', 
        'created', 
        'status', 
        'payment_method',
        'payment_method_nonce',
        'transaction_id',
        'amount',
        'description',
        'email',
        ]
    readonly_fields = [
        'transaction_braintree_url',
        'tariff_type', 
        'created', 
        'payment_method',
        'payment_method_nonce',
        'transaction_id',
        'amount',
        'description',
        'email',
        ]
    list_display = ['email', 'tariff_type', 'created', 'status']
    actions = None

    # def has_delete_permission(self, request, obj=None):
    #     return False

    def transaction_braintree_url(self, obj):
        if settings.BRAINTREE_PRODUCTION:
            return 'no production url yet'
        else:
            url = 'https://sandbox.braintreegateway.com/' \
                'merchants/%s/transactions/%s' % (settings.MERCHANT_ID, obj.transaction_id)
            return '<a href="%s" target="_blank"> %s </a>' % (url, url)
    transaction_braintree_url.allow_tags = True

class ClientTokenAdmin(admin.ModelAdmin):
    pass

class PaidTariffAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(Payment, PaymentAdmin)
admin.site.register(ClientToken, ClientTokenAdmin)
admin.site.register(PaidTariff, PaidTariffAdmin)