# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib import messages

import braintree

from models import Payment

def get_price(ttype):
    price = settings.TARIFF.get(ttype, None)
    return None if not price else price['price']

class PaymentCreateView(TemplateView):
    model = Payment
    fields = ['first_name', 'last_name', 'email']
    template_name = 'payment/create.html'

    def get(self, request, ttype):
        if not get_price(ttype):
            return redirect('support:tariff_select')
        return super(PaymentCreateView, self).get(request, ttype=ttype)

    def get_context_data(self, **kwargs):
        context = super(PaymentCreateView, self).get_context_data(**kwargs)
        context['price'] = get_price(kwargs.get('ttype')) + ' Kč'
        return context

    # create payment and save to db
    def post(self, request, ttype):
        payment_method_nonce = request.POST.get('payment_method_nonce', None)
        if not payment_method_nonce:
            return redirect("payment:create", ttype=ttype)

        price = get_price(ttype)
        if not price:
            return redirect('support:tariff_select')

        result = braintree.Transaction.sale({
            "amount": price,
            "payment_method_nonce": payment_method_nonce,
            "options": {
                "submit_for_settlement": True
            }
        })

        if result.is_success:
            transaction = result.transaction
            payment = Payment.objects.create(
                user = request.user,
                tariff_type = ttype,
                status = transaction.status,
                payment_method = transaction.payment_instrument_type,
                payment_method_nonce = payment_method_nonce,
                amount = get_price(ttype),
                description = 'Payment for tariff %s' % ttype,
                email = request.user.email,
                transaction_id = str(transaction.id)
            )
        elif result.errors:
            # messages.add_message(request, messages.ERROR, result.errors.deep_errors)
            print result.errors.deep_errors
        else:
            print result.transaction.processor_settlement_response_code
            print result.transaction.processor_settlement_response_text

        return redirect(request.META['HTTP_REFERER'])