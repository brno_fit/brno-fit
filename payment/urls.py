from django.conf.urls import url
from views import PaymentCreateView

urlpatterns = [
    url(r'^create/(?P<ttype>[\w]+)', PaymentCreateView.as_view(), name='create'),
]
