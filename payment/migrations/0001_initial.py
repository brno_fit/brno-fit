# -*- coding: utf-8 -*-
# Generated by Django 1.10a1 on 2016-06-18 11:16
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientToken',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.TextField()),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PaidTariff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tariff_type', models.CharField(choices=[('short', 'short'), ('big', 'big'), ('medium', 'medium')], max_length=40)),
                ('valid_from', models.DateField()),
                ('valid_to', models.DateField()),
                ('actual', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tariff_type', models.CharField(choices=[('short', 'short'), ('big', 'big'), ('medium', 'medium')], max_length=40)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('authorization_expired', 'authorization_expired'), ('authorized', 'authorized'), ('authorizing', 'authorizing'), ('settlement_pending', 'settlement_pending'), ('settlement_confirmed', 'settlement_confirmed'), ('settlement_declined', 'settlement_declined'), ('failed', 'failed'), ('gateway_rejected', 'gateway_rejected'), ('processor_declined', 'processor_declined'), ('settled', 'settled'), ('settling', 'settling'), ('submitted_for_settlement', 'submitted_for_settlement'), ('voided', 'voided')], max_length=30)),
                ('payment_method', models.CharField(choices=[('credit_card', 'credit_card'), ('paypal_account', 'paypal_account'), ('apple_pay_card', 'apple_pay_card'), ('coinbase_account', 'coinbase_account'), ('venmo_account', 'venmo_account'), ('android_pay_card', 'android_pay_card')], max_length=20)),
                ('payment_method_nonce', models.TextField()),
                ('amount', models.CharField(max_length=10)),
                ('transaction_id', models.CharField(max_length=256)),
                ('description', models.TextField()),
                ('email', models.EmailField(max_length=40)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='paidtariff',
            name='payment',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='payment.Payment'),
        ),
        migrations.AddField(
            model_name='paidtariff',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
