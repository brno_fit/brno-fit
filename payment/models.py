from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

import datetime

TARIFF = {
	(_('short'), 'short'),
	(_('medium'), 'medium'),
	(_('big'), 'big'),
}

PAYMENT_STATUS = (
	(_("authorization_expired"), "authorization_expired"), 
    (_("authorized"), "authorized"), 
    (_("authorizing"), "authorizing"), 
    (_("settlement_pending"), "settlement_pending"), 
    (_("settlement_confirmed"), "settlement_confirmed"), 
    (_("settlement_declined"), "settlement_declined"), 
    (_("failed"), "failed"), 
    (_("gateway_rejected"), "gateway_rejected"), 
    (_("processor_declined"), "processor_declined"), 
    (_("settled"), "settled"), 
    (_("settling"), "settling"), 
    (_("submitted_for_settlement"), "submitted_for_settlement"), 
    (_("voided"), "voided"), 
)

PAYMENT_METHOD = (
    (_("credit_card"), "credit_card"), 
    (_("paypal_account"), "paypal_account"), 
    (_("apple_pay_card"), "apple_pay_card"), 
    (_("coinbase_account"), "coinbase_account"), 
    (_("venmo_account"), "venmo_account"), 
    (_("android_pay_card"), "android_pay_card"), 
)

class ClientToken(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
	token = models.TextField()
	last_updated = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return '%s - %s...' % (self.user.email, self.token[:20])

class PaidTariff(models.Model):
	tariff_type = models.CharField(choices=TARIFF, max_length=40)
	valid_from = models.DateField()
	valid_to = models.DateField()
	payment = models.OneToOneField('Payment')
	actual = models.BooleanField(default=False)
	user = models.ForeignKey(User)

class Payment(models.Model):
	user = models.ForeignKey(User)
	tariff_type = models.CharField(choices=TARIFF, max_length=40)
	created = models.DateTimeField(auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True)
	status = models.CharField(max_length=30, choices=PAYMENT_STATUS)
	payment_method = models.CharField(max_length=20, choices=PAYMENT_METHOD)
	payment_method_nonce = models.TextField()
	amount = models.CharField(max_length=10)
	transaction_id = models.CharField(max_length=256)
	description = models.TextField()
	email = models.EmailField(max_length=40)

	def __unicode__(self):
		return '%s - %s - %s - %s' % (self.user.email, self.status, 
			self.tariff_type, self.created)