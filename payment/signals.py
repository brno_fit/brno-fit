from allauth.account.signals import user_signed_up
from django.dispatch import receiver
from models import ClientToken
import braintree


@receiver(user_signed_up)
def create_client_token(sender, **kwargs):
	client_id = braintree.ClientToken.generate()
	user = kwargs.get('user', None)
	token, created = ClientToken.objects.get_or_create(user=user)
	token.token = client_id
	token.save()