from django import forms
from functions import get_or_none
from cities.models import City
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _


class CityField(forms.ModelChoiceField):

    def to_python(self, value):
        return get_or_none(City, pk=value) if value else None

    def validate(self, value):
        if not value:
            raise forms.ValidationError(_('City is required'))

class PasswordField(forms.CharField):

    def __init__(self, *args, **kwargs):
        self.widget = forms.PasswordInput
        super(PasswordField, self).__init__(*args, **kwargs)

class UserProfileForm(forms.ModelForm):
    old_password = PasswordField(max_length=40, required=False)
    new_password = PasswordField(max_length=40, required=False)

    class Meta:
        model = User
        fields = ['email']