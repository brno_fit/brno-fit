from django.dispatch import receiver
from allauth.account.signals import user_signed_up

@receiver(user_signed_up)
def username_to_email(sender, **kwargs):
    user = kwargs.get('user')
    user.username = user.email
    user.save()