from django.conf.urls import url
from views import (
    HomeView, get_cities, remove, 
    UserUpdateView, TariffSelectView
)
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^get_cities/', get_cities),
    url(r'^remove/(?P<gym_pk>[\d]+)/(?P<trainer_pk>[\d]+)', remove, name='remove'),
    url(r'^user/update/(?P<pk>[\d]+)', UserUpdateView.as_view(), name='user_update'),
    url(r'^tariff/select/', TariffSelectView.as_view(), name='tariff_select'),
]
