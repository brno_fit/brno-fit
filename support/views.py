from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView, CreateView
from django.template.response import TemplateResponse
from django.contrib.auth.mixins import LoginRequiredMixin

from django.conf import settings

from forms import UserProfileForm
from cities.models import City
from trainer.models import Trainer
from trainer.forms import TrainerFilterForm
from gym.models import Gym
from trainer.models import Trainer
from functions import get_or_none

class TariffSelectView(LoginRequiredMixin, TemplateView):
    template_name = 'support/tariffs.html'

    def get_context_data(self, **kwargs):
        context = super(TariffSelectView, self).get_context_data(**kwargs)
        context['tariffs'] = settings.TARIFF
        return context

class UserUpdateView(UpdateView):
    model = User
    form_class = UserProfileForm

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        old_password = cleaned_data.get('old_password')
        new_password = cleaned_data.get('new_password')
        if new_password:
            if old_password and self.object.check_password(old_password):
                self.object.set_password(new_password)
        return super(UserUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.redirect(requst.META['HTTP_REFERER'])


class TariffSelectView(LoginRequiredMixin, TemplateView):
    template_name = 'support/tariffs.html'

    def get_context_data(self, **kwargs):
        context = super(TariffSelectView, self).get_context_data(**kwargs)
        context['tariffs'] = settings.TARIFF
        return context

class UserUpdateView(UpdateView):
    model = User
    form_class = UserProfileForm

    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        old_password = cleaned_data.get('old_password')
        new_password = cleaned_data.get('new_password')
        if new_password:
            if old_password and self.object.check_password(old_password):
                self.object.set_password(new_password)
        return super(UserUpdateView, self).form_valid(form)

    def get_success_url(self):
        return self.redirect(requst.META['HTTP_REFERER'])

@login_required
def remove(request, trainer_pk, gym_pk):
    gym = get_or_none(Gym, pk=gym_pk)
    trainer = get_or_none(Trainer, pk=trainer_pk)
    if gym and trainer:
        gym.trainers.remove(trainer)
        return HttpResponse('removed')
    return HttpResponse('error')


def trainer_filter(get):
    default_queryset = Trainer.objects.all()
    fquery = Q()
    if get.get('name', None):
        fquery = Q(user__first_name__icontains=get['name'])
        fquery = fquery | Q(user__last_name__icontains=get['name'])
    if get.get('country', None):
        fquery = fquery & Q(country=get['country'])
    if get.get('city', None):
        fquery = fquery & Q(city=get['city'])
    return default_queryset.filter(fquery).order_by('-rate')


class HomeView(TemplateView):
    template_name = 'support/home.html'
    filter_form = TrainerFilterForm

    def get_context_data(self, request):
        context = dict()
        context['trainer_list'] = trainer_filter(request.GET)
        context['trainer_list'] = ListView(User).get_list(
            request.GET, context['trainer_list'])
        context['filter_form'] = self.filter_form()
        return context

    def get(self, request):
        context = self.get_context_data(request=request)
        return render(request, self.template_name, context)


def get_cities(request):
    country = request.GET.get('pk')
    cities = City.objects.filter(
        country__id=country).values_list('id', 'name_std')
    if cities:
        return JsonResponse({"data": list(cities)})


class ListView():
    per_page = 10
    fields = '__all__'
    detail_view = ''

    def __init__(self, model, per_page=None):
        self.model = model

        if per_page:
            self.per_page = per_page

    def get_list(self, get, queryset):
        trainer_list = {}

        if not queryset:
            return None

        page = get.get('page')
        paginator = Paginator(queryset, self.per_page)

        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        trainer_list['objects'] = objects

        return trainer_list