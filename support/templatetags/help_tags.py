from django import template
from django.utils.text import slugify
from django.template import Library
from django.core.urlresolvers import resolve, reverse
from django.utils.translation import activate, get_language

register = template.Library()


@register.simple_tag(takes_context=True)
def change_lang(context, lang=None, *args, **kwargs):
    """
    Get active page's url by a specified language
    Usage: {% change_lang 'en' %}
    """
    path = context['request'].path
    url_parts = resolve(path)
    url = path
    cur_language = get_language()
    try:
        activate(lang)
        url = reverse(url_parts.view_name, kwargs=url_parts.kwargs)
    finally:
        activate(cur_language)
    return "%s" % url

@register.filter
def query_cut(value, to):
    return value[:to]

@register.filter
def normalize(value):
    slug = slugify(value).replace('_', ' ')
    return slug.title()


@register.filter
def get_value(obj, attr):
    attrs = attr.split('.')
    curr_value = obj
    for a in attrs:
        curr_value = getattr(curr_value, a)
    if len(curr_value) > 50:
        return curr_value[:50] + '...'
    return curr_value


@register.filter
def cut(value, size):
    if len(value) > size:
        return value[:int(size)] + '...'
    else:
        return value


@register.filter
def not_none(value):
    return 1 if not value else None


@register.filter
def none(value):
    return None if not value else value

@register.filter
def empty_str(value):
    return '' if not value else value

@register.filter
def trange(left, right=1):
    return range(right, int(left) + 1)


@register.filter
def avatar(photos):
    avatar_photo = photos.filter(avatar=True)
    if avatar_photo:
        return avatar_photo.get().photo.url
    return None


@register.filter
def average_rating(object):
    try:
        return sum(object.reviews.values_list('rate', flat=True)) / object.reviews.count()
    except Exception, e:
        return 0


@register.filter
def get_full_url(url):
    return "http://127.0.0.1:8000" + url