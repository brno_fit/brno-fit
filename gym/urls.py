from django.conf.urls import url
from views import (
    GymProfile, GymProfileUpdate, GymRegisterView, GymListView,
    photo_delete, photo_save, get_gym_list
)

urlpatterns = [
    url(r'list$', GymListView.as_view(), name='list'),
    url(r'json$', get_gym_list),
    url(r'detail/(?P<pk>[\d]+)/$', GymProfile.as_view(), name='detail'),
    url(r'settings/$', GymProfileUpdate.as_view(), name='settings'),
    url(r'register/$', GymRegisterView.as_view(), name='register'),  # ajax
    url(r'photo/save/$', photo_save, name='photo_save'),
    url(r'photo/delete/(?P<pk>[\d]+)/$', photo_delete, name='photo_delete'),
]
