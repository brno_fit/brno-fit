from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from models import Address, Gym
from support.functions import get_or_none
from support.forms import CityField
from cities.models import Country, City
from django.utils.translation import ugettext as _

class GymFilterForm(forms.Form):
    name = forms.CharField(
        max_length=30, 
        widget=forms.TextInput(attrs={'placeholder': _('Gym name')}), 
        required=False
    )
    country = forms.ModelChoiceField(
        queryset=Country.objects, 
        to_field_name='pk', empty_label=_('Country'),
        required=False
    )
    city = forms.ModelChoiceField(
        queryset=City.objects.none(), 
        empty_label=_('Please, pick a country'),
        required=False
    )


class GymUpdateForm(forms.ModelForm):

    class Meta:
        model = Gym
        fields = ['name', 'homepage_url']


class AddressRegisterForm(forms.ModelForm):
    city = CityField(queryset=City.objects.none())

    class Meta:
        model = Address
        fields = '__all__'
        exclude = ['point']