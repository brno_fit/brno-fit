from django.db.models import Q
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.models import User
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.contrib.gis.geos import Point
from django.utils.translation import ugettext as _
from django.template.loader import render_to_string
from django.forms import modelformset_factory
from datetime import datetime

from support.functions import get_or_none
from support.forms import UserProfileForm
from cities.models import Country, City
from models import Gym, Address, GymPhoto
from forms import (
    AddressRegisterForm, GymUpdateForm, GymFilterForm
)
from support.views import ListView
from invite.models import Notification
from openhours.models import OpenHours


def get_gym_list(request):
    if request.method == 'GET':
        gyms = Gym.objects.all()[:5]
    else:
        #ids = request.POST.getlist('ids[]')
        default_queryset = Gym.objects.all()
        fquery = Q()
        if request.POST.get('name', None):
            fquery = Q(name__icontains=request.POST['name'])
        if request.POST.get('country', None):
            fquery = fquery & Q(address__country=request.POST['country'])
        if request.POST.get('city', None):
            fquery = fquery & Q(address__city=request.POST['city'])
        gyms = default_queryset.filter(fquery)

    gyms_list = list()
    if gyms:
        for gym in gyms:
            if gym.address.point:
                avatar_photo = gym.photos.filter(avatar=True)
                temp = dict()
                temp['pk'] = gym.pk
                temp['name'] = gym.name
                temp['address'] = dict()
                temp['address']['coords'] = gym.address.point.coords
                temp['address']['country'] = gym.address.country.name
                temp['address']['city'] = gym.address.city.name
                temp['address']['street'] = gym.address.street
                temp['avatar'] = avatar_photo.get(
                ).photo.url if avatar_photo else "/static/image/not_found.png"
                gyms_list.append(temp)
        return JsonResponse({"data": gyms_list})
    return JsonResponse({"data": list()})


def gym_filter(get):
    default_queryset = Gym.objects.all()
    fquery = Q()
    if get.get('name', None):
        fquery = Q(name__icontains=get['name'])
    if get.get('country', None):
        fquery = fquery & Q(address__country=get['country'])
    if get.get('city', None):
        fquery = fquery & Q(address__city=get['city'])
    return default_queryset.filter(fquery)


class GymListView(TemplateView):
    template_name = 'gym/main.html'
    filter_form = GymFilterForm

    def get_context_data(self, request):
        context = dict()
        context['gym_list'] = gym_filter(request.GET)
        context['gym_list'] = ListView(Gym).get_list(
            request.GET, context['gym_list'])
        context['filter_form'] = self.filter_form()
        return context

    def get(self, request):
        context = self.get_context_data(request=request)
        return render(request, self.template_name, context)


class GymRegisterView(LoginRequiredMixin, CreateView):
    model = Gym
    form_address = AddressRegisterForm
    template_name = 'gym/register.html'
    fields = ['name', 'homepage_url']

    def get_context_data(self, **kwargs):
        context = super(GymRegisterView, self).get_context_data(**kwargs)
        context['address'] = self.form_address()
        return context

    def form_valid(self, form):
        address = self.form_address(self.request.POST)
        if address.is_valid():
            address = address.save()
            form.instance.address = address
            form.instance.user = self.request.user
        else:
            return self.form_invalid(form)
        return super(GymRegisterView, self).form_valid(form)

    def get_absolute_url(self):
        return reverse('gym:settings', args=(self.object.pk,))

class GymProfile(DetailView):
    template_name = 'gym/detail.html'
    model = Gym
    fields = '__all__'

    def get_context_data(self, **kwargs):
        context = super(GymProfile, self).get_context_data(**kwargs)
        user = self.request.user
        now = datetime.now()
        context['opened'] = self.object.openhours_set.filter(weekday=now.weekday() + 1, 
            from_hour__lte=now.strftime("%H:%M"),
            to_hour__gte=now.strftime("%H:%M")).exists()
        if user.is_authenticated() and not self.request.is_ajax():
            if (getattr(user, 'trainer', None) 
                and not get_or_none(Notification, gym=self.object, trainer=user.trainer, ntype=0) 
                and not user.trainer.gym_trainers.filter(pk=self.object.pk).exists()):
                context['invite'] = True
        return context

    def get(self, request, pk):
        if request.is_ajax():
            self.object = self.get_object()
            context = self.get_context_data(pk=pk)
            html = render_to_string('gym/content_detail.html', context)
            return HttpResponse(html)
        else:
            return super(GymProfile, self).get(request, pk)


class GymProfileUpdate(LoginRequiredMixin, UpdateView):
    template_name = 'gym/settings.html'
    model = Gym
    fields = ['name', 'homepage_url']
    forms = {
        'form': GymUpdateForm,
        'address': AddressRegisterForm,
    }

    def get_context_data(self, **kwargs):
        context = super(GymProfileUpdate, self).get_context_data(**kwargs)
        context['address'] = self.forms['address'](
            instance=self.object.address)
        context['my_city'] = self.object.address.city
        context['cities'] = City.objects.filter(
            country=self.object.address.country)
        context['user_form'] = UserProfileForm(instance=self.request.user)

        hours = OpenHours.objects.filter(gym=self.request.user.gym)
        HoursFormset = modelformset_factory(OpenHours, exclude=['gym', 'id',], max_num=7, extra=7)
        context['hours_form'] = HoursFormset(queryset=hours)
        return context

    def get_object(self):
        return Gym.objects.get(user=self.request.user)

    def form_valid(self, form):
        from django.contrib.gis.geos import GEOSGeometry
        address = AddressRegisterForm(self.request.POST) 
        if address.is_valid():
            address = AddressRegisterForm(self.request.POST, instance=self.object.address)
            x, y = self.request.POST.get('point').split()
            print x, y
            address = address.save(commit=False)
            address.point =  GEOSGeometry('POINT(%s %s)' % (x, y))
            address.save()
            print address.point
        else:
            print address.errors
        form.save()
        return redirect('gym:settings')


@login_required
def photo_save(request):
    if request.user.gym.photos.count() >= 5:
        return HttpResponse(_('Max is 5'))
    avatar = get_or_none(GymPhoto, gym=request.user.gym, avatar=True)
    photo = GymPhoto(gym=request.user.gym, photo=request.FILES['file'])
    if not avatar:
        photo.avatar = True
    photo.save()
    return HttpResponse(_('Uploaded'))


@login_required
def photo_delete(request, pk):
    photo = get_or_none(GymPhoto, gym=request.user.gym, pk=pk)
    if photo:
        photo.delete()
        return HttpResponse(_('Deleted'))
    return HttpResponse(_('Photo does not exist'))


@login_required
def set_avatar(request, pk):
    photo = get_or_none(GymPhoto, gym=request.user.gym, pk=pk)
    old_avatar = get_or_none(GymPhoto, gym=request.user.gym, avatar=True)
    if old_avatar:
        old_avatar.avatar = False
        old_avatar.save()
    photo.avatar = True
    photo.save()
    return HttpResponse(_('Avatar set'))
