from django.contrib import admin
from models import Gym, GymPhoto, Address
from forms import AddressRegisterForm


class GymAdmin(admin.ModelAdmin):
    filter_horizontal = ['trainers', ]


class AddressAdmin(admin.ModelAdmin):
    form_class = AddressRegisterForm

    class Media:
        js = ('js/cities.js',)


class GymPhotoAdmin(admin.ModelAdmin):
    pass

admin.site.register(Gym, GymAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(GymPhoto, GymPhotoAdmin)
