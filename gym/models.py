from __future__ import unicode_literals
from django.contrib.gis.db import models
from trainer.models import Trainer
from django.contrib.auth.models import User
from cities.models import Country, City
from imagekit.models import ProcessedImageField


class Address(models.Model):
    country = models.ForeignKey(Country)
    city = models.ForeignKey(City)
    street = models.CharField(max_length=100)
    point = models.PointField(null=True)

    def __unicode__(self):
        return "%s, %s, %s" % (self.street, self.city, self.country)


class Gym(models.Model):
    name = models.CharField(max_length=100)
    address = models.ForeignKey('Address', related_name='gym_address')
    homepage_url = models.URLField(null=True)
    trainers = models.ManyToManyField(Trainer, related_name='gym_trainers')
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='gym')
    active = models.BooleanField(default=True)
    can_show = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.name


class GymPhoto(models.Model):
    gym = models.ForeignKey(Gym, related_name='photos')
    photo = ProcessedImageField(
        upload_to='gym/', format='JPEG', options={'quality': 60})
    avatar = models.BooleanField(default=False)

    def __unicode__(self):
        return self.gym.user.email
