from django.conf.urls import url
from views import ShowMessagesView, SendMessageView 

urlpatterns = [
    url(r'show$', ShowMessagesView.as_view(), name='show'),
    url(r'send/(?P<pk>[\d]+)/$', SendMessageView.as_view(), name='send'),
]
