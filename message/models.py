from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from phonenumber_field.modelfields import PhoneNumberField

class Message(models.Model):
	user = models.ForeignKey(User)
	first_name = models.CharField(max_length=100)
	email = models.EmailField(max_length=100)
	phone = PhoneNumberField()
	text = models.TextField(blank=False)

	def __unicode__(self):
		return '%s %s %s' % (self.user.email, self.first_name, self.text[:20])