from django import forms
from models import Message


class MessageCreateForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['first_name', 'email', 'phone', 'text']