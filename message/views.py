from django.shortcuts import render, get_object_or_404, render_to_response
from django.views.generic.edit import CreateView
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.http import Http404, JsonResponse
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.template.loader import get_template
from django.template import Context
from django.utils.translation import ugettext as _

from models import Message
from forms import MessageCreateForm


class SendMessageView(CreateView):
    model = Message
    form_class = MessageCreateForm 
    template_name = 'message/create.html'

    def get_context_data(self, *args, **kwargs):
        context = super(SendMessageView, self).get_context_data(*args, **kwargs)
        context['pk'] = self.kwargs['pk']
        return context
        
    def form_valid(self, form):
        user_id = self.kwargs.get('pk', None)
        if not user_id:
            return JsonResponse({
                'sended' : False,
                'message' : _('Can\'t find user to send email') 
                })

        user = get_object_or_404(User, pk=user_id)

        subject = _("New message from trainers.com")
        to = [user.email]
        from_email = 'trainer@gmail.com'

        ctx = {
            'user': user,
            'data': form.cleaned_data
        }

        message = get_template('message/email.html').render(Context(ctx))
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()

        form.instance.user = user
        form.save()
        return JsonResponse({
            'sended' : True,
            'message' : _('Message send')
            })

    def form_invalid(self, form):
        print form.errors
        return JsonResponse({
                'sended' : False,
                'message' : _('Please check all fields.'),
                'errors' : str(form.errors)
                })

    def get_success_url(self, **kwargs):
        return redirect(self.request.META['HTTP_REFERER'])

class ShowMessagesView(LoginRequiredMixin, ListView):
    model = Message
    template_name = 'message/show.html'

    def get_queryset(self, *args, **kwargs):
        return Message.objects.filter(user=self.request.user)